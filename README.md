# Process API

This restful Flask-API is built to call background-processes on a server.

**Warning!** This app is far from productive and could probably breaks some things!

## Usage

```bash
virtualenv venv
venv/bin/pip install -r requirements.txt
export FLASK_APP=app.py
venv/bin/flask run
```