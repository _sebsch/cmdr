from json import loads

from tests import ApiTestCase


class TestRootResource(ApiTestCase):
    def test_get(self):
        url = self.resources.get("rootresource", None)
        self.assertIsNot(url, None)
        response = self.app.get(url)

        self.assertEqual(response.status_code, 200)

    def test_return_types(self):
        url = self.resources.get("rootresource", None)
        self.assertIsNot(url, None)
        response = self.app.get(url)

        payload = loads(response.data)
        self.assertIsInstance(payload, list)

        for content in payload:
            self.assertIsInstance(content, dict)
