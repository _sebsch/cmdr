from unittest import TestCase

from api import api, db, app


class ApiTestCase(TestCase):

    def setUp(self):
        app.config['TESTING'] = True
        app.config['DEBUG'] = True

        self.app = app.test_client()
        self.api = api
        self.db = db

        self.resources = {x.endpoint: f"{x}" for x in app.url_map.iter_rules() if x.endpoint in list(api.endpoints)}

        with app.app_context():
            db.drop_all()
            db.create_all()

    def tearDown(self):
        pass
