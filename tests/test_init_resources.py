from api import app
from tests import ApiTestCase


class TestInitResources(ApiTestCase):

    @staticmethod
    def parse_url_blocks(url):
        url = url.replace("<int:sleep_seconds>", "7")
        url = url.replace("<string:register_str>", "abcde")
        return url

    def test_all_routes_awailable(self):
        resources = {
            x.endpoint: f"{x}"
            for x in app.url_map.iter_rules()
            if x.endpoint in list(self.api.endpoints)
        }

        for r, u in resources.items():
            u = self.parse_url_blocks(u)
            response = self.app.get(u)
            print(f"{r} -> {u} [{response.status_code}]")
            self.assertNotEqual(response.status_code, 404)
