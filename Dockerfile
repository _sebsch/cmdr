FROM python:3.7-alpine
RUN apk add --no-cache linux-headers musl-dev gcc
RUN apk add --no-cache py-virtualenv
RUN apk add --no-cache git

RUN mkdir -p /opt/cmdr
RUN git clone https://gitlab.com/_sebsch/cmdr.git  /opt/cmdr
RUN pip install -r /opt/cmdr/requirements.txt

ENTRYPOINT ["python"]
CMD ["/opt/cmdr/cmdr.py"]
