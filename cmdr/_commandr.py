""" CommandR -- An interface to the commandline. """

from dataclasses import dataclass
from subprocess import Popen, PIPE
from typing import Dict, Callable, List, Tuple


@dataclass
class ProcessReturn:
    command: List[str]
    std_out: bytes
    std_err: bytes
    return_code: int

    """ This is the data-structure of one called process. """

    def out(self):
        return str(self.std_out, 'utf-8').splitlines()

    def err(self):
        return str(self.std_err, 'utf-8').splitlines()


class CommandrEntry:
    """Build up a command."""

    def __init__(self, binary: str, params: str):
        self.binary = binary
        self.params = params

    def __call__(self, *args, **kwargs):
        p = self.params.format(**kwargs)
        return [self.binary] + p.split(';')


class CommandR:
    """ Execute commands on the System.

            @TODO Yes this is as dangerous as it sounds. Find a solution!

            The execution is parsed through a mapper. So the definition
            has to be done before the request is send. Only the commands
            are defined before will be executed.

    """

    def __init__(self, language: Dict):
        self.lang_mapper = language

    @staticmethod
    def __exec(cmd: List) -> Tuple[bytes, bytes, int]:
        """ Inner interface to the process-call. """

        ps = Popen(cmd, stdout=PIPE, stderr=PIPE)
        out, err = ps.communicate()
        return_code = ps.returncode

        return out, err, return_code

    def __getitem__(self, key: str) -> Callable[[Dict], ProcessReturn]:
        """ The obj[key]-Interface.

                :param key:
                :return: Inner function
        """

        def __call(**kwargs) -> ProcessReturn:
            """ The functional outer interface to the call.

                    This does actually two things:

                    1.  building a command-string out of the parameters key and word.
                    2.  executing the command-string via the inner process-interface
            """

            command = self.lang_mapper[key](**kwargs)
            out, err, return_code = self.__exec(command)

            return ProcessReturn(command, out, err, return_code)

        return __call


def main():
    cmdr = CommandR({
        'LISTDIR': CommandrEntry('ls', '-L;-i;-l;-a;-h;{directory}'),
    })

    print(cmdr['LISTDIR'](directory='/tmp'))


if __name__ == '__main__':
    main()
