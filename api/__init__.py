from api._api import create_api, create_app, init_db

app = create_app('api.cfg')
db = init_db(app)
api = create_api(app)


def register_resources(api):
    """Initialize the api-resources."""
    from ._resources import RootResource
    from ._resources import CmdR

    api.add_resource(RootResource, '/')
    api.add_resource(CmdR, '/cmdr/<string:CMD>/<PARAM>')


register_resources(api)
