from . import db


class ThreadLock(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    resource = db.Column(db.String(80), unique=True, nullable=False)
    status = db.Column(db.String(120), nullable=False)
    since = db.Column(db.DateTime)

    def __repr__(self):
        return "<Resource %r is %r>" % (self.resource, self.status)
