from flask_sqlalchemy import SQLAlchemy

db: SQLAlchemy = SQLAlchemy()

from ._thread_lock_model import ThreadLock
