from flask import Flask
from flask_restful import Api


def init_db(app):
    """Creates a flask-SQLAlchemy db-object"""
    from .models import db

    db.init_app(app)

    with app.app_context():
        db.drop_all()
        db.create_all()
    return db


def create_app(config_filename):
    """Creates a flask app"""

    app = Flask(__name__, static_folder=None)
    app.config.from_pyfile(config_filename)

    return app


def create_api(app):
    """creates a flask-restful api"""

    api = Api(app)

    return api
