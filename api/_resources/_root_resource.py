from flask import jsonify
from flask_restful import Resource

from .. import app


class RootResource(Resource):
    """The root-page of the API.
        Giving some information.
    """

    def get(self):
        endpoint = lambda r: r.endpoint
        methods = lambda r: ','.join(sorted(r.methods))

        return jsonify(
            [{
                'Url:': str(rule),
                'Endpoint': endpoint(rule),
                'Methods': methods(rule),

            } for rule in app.url_map.iter_rules()])
