from flask_restful import Resource, marshal_with, fields, reqparse

from cmdr import CommandR, CommandrEntry

parser = reqparse.RequestParser()
parser.add_argument('directory')

resource_fields = {
    'command': fields.String,
    'std_out': fields.List(fields.String),
    'std_err': fields.List(fields.String),
    'return_code': fields.Integer,
}

cmdr = CommandR({
    'LS': CommandrEntry('ls', '-Lilah;{directory}'),
})


class CmdRDao:
    def __init__(self, cmd, param):
        args = parser.parse_args()
        command_return = cmdr[cmd](**param)

        self.command = " ".join(command_return.command)
        self.std_out = command_return.out()
        self.std_err = command_return.err()
        self.return_code = command_return.return_code


def tools(cmd, param):
    p = {{
        'LS': {'directory': '/tmp'}
    }}[cmd][param]

    return p


class CmdR(Resource):
    @marshal_with(resource_fields)
    def get(self, CMD, PARAM):
        param = tools(CMD, PARAM)

        return CmdRDao(CMD, param)
