from flask_restful import Resource, marshal_with
from flask_restful.fields import String, Boolean

from api import api, db

app = api.app

resource_fields = {
    'id': String,
    'service': String,
    'return_message': {
        'message': String,
        'succeed': Boolean,
    }
}


def get_or_create(session, model, **kwargs):
    instance = session.query(model).filter_by(**kwargs).first()

    if instance:
        return instance
    else:
        instance = model(**kwargs)
        session.add(instance)
        session.commit()
        return instance


def start_if_not_working(session, service):

    if service.status == 'working':
        return dict(
            message=f"{service} is working since {service.since} [{service.id}]",
            succeed=False
        )

    service.status = 'working'
    session.commit()

    return dict(
        message=f"{service.resource} started working {service.since} [{service.id}]",
        succeed=True
    )


class TestDao:
    def __init__(self, resource):
        self.id = resource

        session = db.create_scoped_session(
            options={
                'bind': db.get_engine(app, None),
                'binds': {},
            }
        )
        service = get_or_create(session, db.Model.ThreadLock, resource=resource)
        self.return_message = start_if_not_working(session, service)


class Test(Resource):
    @marshal_with(resource_fields)
    def get(self, resource):
        TestDao(resource=resource)
